<?php
/*
 Template Name: work_posts
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/

?>

<?php get_header(); ?>


<?php
  /*
  施工事例情報アーカイブ用
  */

  //フィールド名一覧
  $works_title = get_field('works_title');//事例タイトル
  $result_txt01 = get_field('result_txt01');//結果コメント1
  $result_txt02 = get_field('result_txt02');//結果コメント2
  $photo_main = get_field('photo_main');//メイン画像
  $area = get_field('area');//都道府県
  $construction = get_field('construction');//構造
  $floor = get_field('floor');//間取り
  $occupied_area = get_field('occupied_area');//専有面積
  $garary_photo01_before = get_field('garary_photo01_before');//ギャラリー1_施工前写真
  $garary_photo01_after = get_field('garary_photo01_after');//ギャラリー1_施工後写真
  $garary_txt01 = get_field('garary_txt01');//ギャラリー1コメント
  $garary_photo02_before = get_field('garary_photo02_before');//ギャラリー2_施工前写真
  $garary_photo02_after = get_field('garary_photo02_after');//ギャラリー2_施工後写真
	$garary_txt02 = get_field('garary_txt02');//ギャラリー2コメント
	
	//合計施工数取得したい
	$count_post = wp_count_posts('work');
	$works_num = $count_post->publish;
  
?>
		

	<section class="c-section p-work-list-header">
		<div class="c-section-1280">
			<div class="c-header">
          <h1 class="c-title-regular">施工事例</h1>
          <p class="c-read">サムズアップが手掛けた施工事例をご紹介いたします。賃貸マンションのリフォームを中心にマンション経営でお悩みのオーナー様に、賃貸市場のニーズに合わせたリフォームをご提案いたします。借主様がちょっと自慢したくなるようなお部屋にしませんか？</p>
				</div>
		</div>
	</section>

	<section class="c-section p-work-list-content">
		<div class="c-section-1280">
			<ul class="c-box3-list u-clearfix c-base-m-b">

			<?php
			//絞り込みした記事を表示するループ
			$paged = get_query_var('paged') ? get_query_var('paged') : 1 ;
			$args = array(
			'post_type' => 'work',//投稿タイプを設定
			'posts_per_page' => 9,//表示件数設定
			'paged' => $paged,
			);
			$wp_query = new WP_Query();
			$wp_query->query( $args );
			while ( $wp_query->have_posts() ) : $wp_query->the_post();
			?>
			
				<li class="c-box3-list__item p-work-list__item" data-scroll="toggle(.c-fadein, .c-fadeout) once">
					<a href="<?php the_permalink() ?>">
					<?php
						$imgurl = wp_get_attachment_url(get_post_meta($wp_query->post->ID,'photo_main',true));
						if($imgurl){ ?>
						<div class="c-box3-list__image c-work-list__image"><img src="<?php echo wp_get_attachment_url(get_post_meta($wp_query->post->ID,'photo_main',true));?>"></div>
						<?php } ?>
						<h3 class="c-box3-list__title">
							<?php
								$num = 60;
								$worls_title = strip_tags(get_post_meta( $wp_query->post->ID , 'works_title' , true ));
								if ($num < strlen($worls_title)){
										echo mb_strimwidth($worls_title,0,$num,'...','UTF-8');
								}else{
									echo $worls_title;
								}
							?>
						</h3>
					</a>
				</li>

			<?php endwhile; ?>
			</ul>
		</div>
		
		<?php
		if(wp_is_mobile()) { 
			bones_page_navi_sp();
    } else {
			bones_page_navi(); 
		}
		
		wp_reset_query(); ?>
		
		</section>
				
	
<?php get_footer(); ?>
