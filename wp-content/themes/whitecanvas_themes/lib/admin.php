<?php

/**********************************/
/***** 管理画面系 *****/
/**********************************/
 
 /* 自動生成タグ制御 */
//remove_filter('the_content', 'wpautop');// 記事の自動整形を無効にする
remove_filter('the_excerpt', 'wpautop');// 抜粋の自動整形を無効にする

function disable_page_wpautop() {//固定記事のみ自動整形を無効にする
	if ( is_page() ) remove_filter( 'the_content', 'wpautop' );
}
add_action( 'wp', 'disable_page_wpautop' );

/* バージョンアップ通知をcw管理者のみ表示させる */
function update_nag_admin_only() {
global $current_user;
get_currentuserinfo();
    if ( $current_user->ID !='1') {
        remove_action( 'admin_notices', 'update_nag', 3 );
        remove_action( 'admin_notices', 'maintenance_nag', 10 );
    }
}
add_action( 'admin_init', 'update_nag_admin_only' );
 
/*cw管理者以外アップデート表記を非表示 */
global $current_user;
get_currentuserinfo();
    if ( $current_user->ID !='1') {
function hide_admin_items() {
?>
<style type="text/css">#wp-version-message a,#footer-upgrade,#footer-thankyou {display:none !important;}</style>
<?php
}
add_action ('admin_head','hide_admin_items');
}

/* 管理画面のユーザー一覧から権限が管理者以下で管理者の表示を非表示にしたい */
function my_pre_user_query( $uqi ) {
	global $pagenow;
	if( $pagenow == 'users.php' ) {
		$current_user = wp_get_current_user();
		if ( $current_user->roles[0] != 'administrator' ) { 
			global $wpdb;
			$uqi->query_where = str_replace(
				'WHERE 1=1',
				"WHERE 1=1 AND {$wpdb->users}.ID IN (" .
					"SELECT {$wpdb->usermeta}.user_id FROM {$wpdb->usermeta} " .
					"WHERE {$wpdb->usermeta}.meta_key='{$wpdb->prefix}capabilities' " .
					"AND {$wpdb->usermeta}.meta_value NOT LIKE '%administrator%'" .
				')',
				$uqi->query_where
			);
		}
	}
}
add_action( 'pre_user_query','my_pre_user_query' );

/* 管理画面投稿ページに不要オプションを非表示 */
function my_remove_meta_boxes() {
remove_meta_box('authordiv', 'post', 'normal'); // オーサー
remove_meta_box('commentsdiv', 'post', 'normal'); // コメント
remove_meta_box('formatdiv', 'post', 'normal'); // フォーマット
remove_meta_box('postexcerpt', 'post', 'normal'); // 抜粋
remove_meta_box('postimagediv', 'post', 'normal'); // アイキャッチ
remove_meta_box('tagsdiv-post_tag', 'post', 'normal'); // タグ
remove_meta_box('trackbacksdiv', 'post', 'normal'); // トラックバック
}
add_action('admin_menu', 'my_remove_meta_boxes');

/* 特殊文字自動変換回避 */
add_filter( 'run_wptexturize', '__return_false' );

/* 管理画面の【投稿】を【新着情報】に変更 */
function change_post_menu_label() {
global $menu;
global $submenu;
$menu[5][0] = '新着情報';
$submenu['edit.php'][5][0] = '新着情報一覧';
$submenu['edit.php'][10][0] = '新しい新着情報';
$submenu['edit.php'][16][0] = 'タグ';
//echo ";
}
function change_post_object_label() {
global $wp_post_types;
$labels = &$wp_post_types['post']->labels;
$labels->name = '新着情報';
$labels->singular_name = '新着情報';
$labels->add_new = _x('追加', '新着情報');
$labels->add_new_item = '新着情報の新規追加';
$labels->edit_item = '新着情報の編集';
$labels->new_item = '新規新着情報';
$labels->view_item = '新着情報を表示';
$labels->search_items = '新着情報を検索';
$labels->not_found = '記事が見つかりませんでした';
$labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

/* 【新着情報】の記事詳細の階層を変更 */
function add_article_post_permalink( $permalink ) {
    $permalink = '/news' . $permalink;
    return $permalink;
}
add_filter( 'pre_post_link', 'add_article_post_permalink' );
 
function add_article_post_rewrite_rules( $post_rewrite ) {
    $return_rule = array();
    foreach ( $post_rewrite as $regex => $rewrite ) {
        $return_rule['news/' . $regex] = $rewrite;
    }
    return $return_rule;
}
add_filter( 'post_rewrite_rules', 'add_article_post_rewrite_rules' );

/* setting_config実装 */
function my_site_setting_menu(){

  add_menu_page('サイト設定', 'サイト設定', 'manage_options', 'my_site_setting_menu', 'my_site_setting','',4);

}
add_action('admin_menu', 'my_site_setting_menu');



function my_site_setting(){

  if(!get_option('COMPANY_NAME')) add_option('COMPANY_NAME','会社名');
  if(!get_option('COMPANY_KEY')) add_option('COMPANY_KEY','キーワード1,キーワード2');
  if(!get_option('CEO_NAME')) add_option('CEO_NAME','代表者名');
  if(!get_option('POST')) add_option('POST','〒000-0000');
  if(!get_option('ADDRESS')) add_option('ADDRESS','○○県○○市');
//  if(!get_option('MAP')) add_option('MAP','https://www.google.co.jp/maps?q=');
  if(!get_option('MAIN_TEL')) add_option('MAIN_TEL','000-000-0000');

  if(!get_option('TEL')) add_option('TEL','000-000-0000');
  if(!get_option('FAX')) add_option('FAX','000-000-0000');
  if(!get_option('TEL_TIME')) add_option('TEL_TIME','00:00～00:00');
  if(!get_option('COPY')) add_option('COPY','xxxxxxx Co.,Ltd.');


  $action = 'my_site_setting';
  $nonce = wp_create_nonce($action);
  if(wp_verify_nonce($_POST[$nonce], $action)):

    $_POST['COMPANY_NAME'] = trim(mb_convert_kana($_POST['COMPANY_NAME'],'KVs','utf-8'));
	$_POST['COMPANY_KEY'] = trim(mb_convert_kana($_POST['COMPANY_KEY'],'KVs','utf-8'));
    $_POST['CEO_NAME'] = trim(mb_convert_kana($_POST['CEO_NAME'],'KVs','utf-8'));
    $_POST['POST'] = trim(mb_convert_kana($_POST['POST'],'ns','utf-8'));
    if(!preg_match('/〒/',$_POST['POST'])):
      $_POST['POST'] = '〒'.$_POST['POST'];
    endif;
    $_POST['ADDRESS'] = trim(mb_convert_kana($_POST['ADDRESS'],'KVs','utf-8'));
    $_POST['MAP'] = trim(mb_convert_kana($_POST['MAP'],'as','utf-8'));
    $_POST['MAIN_TEL'] = trim(mb_convert_kana($_POST['MAIN_TEL'],'ns','utf-8'));

    $_POST['TEL'] = trim(mb_convert_kana($_POST['TEL'],'ns','utf-8'));
    $_POST['FAX'] = trim(mb_convert_kana($_POST['FAX'],'ns','utf-8'));

    $_POST['TEL_TIME'] = trim(mb_convert_kana($_POST['TEL_TIME'],'ns','utf-8'));
    $_POST['TEL_TIME'] = preg_replace('/\s/','',$_POST['TEL_TIME']);
    $_POST['TEL_TIME'] = preg_replace('/～/',' ～ ',$_POST['TEL_TIME']);

    $_POST['COPY'] = trim(mb_convert_kana($_POST['COPY'],'as','utf-8'));

      if(isset($_POST['COMPANY_NAME'])):
        $errmsg['COMPANY_NAME'] = '';
        if(!$errmsg['COMPANY_NAME']):
          update_option('COMPANY_NAME',$_POST['COMPANY_NAME']);
        endif;
      endif;

      if(isset($_POST['COMPANY_KEY'])):
        $errmsg['COMPANY_KEY'] = '';
        if(!$errmsg['COMPANY_KEY']):
          update_option('COMPANY_KEY',$_POST['COMPANY_KEY']);
        endif;
      endif;
      if(isset($_POST['CEO_NAME'])):
        $errmsg['CEO_NAME'] = '';
        if(!$errmsg['CEO_NAME']):
          update_option('CEO_NAME',$_POST['CEO_NAME']);
        endif;
      endif;

      if(isset($_POST['POST'])):
        $errmsg['POST'] = '';
        if(!$errmsg['POST']):
          update_option('POST',$_POST['POST']);
        endif;
      endif;

      if(isset($_POST['ADDRESS'])):
        $errmsg['ADDRESS'] = '';
        if(!$errmsg['ADDRESS']):
          update_option('ADDRESS',$_POST['ADDRESS']);
        endif;
      endif;

//      if(isset($_POST['MAP'])):
//        $errmsg['MAP'] = '';
//        if(!$errmsg['MAP']):
//          update_option('MAP',$_POST['MAP']);
//        endif;
//     endif;

      if(isset($_POST['MAIN_TEL'])):
        $errmsg['MAIN_TEL'] = '';
        if(!$errmsg['MAIN_TEL']):
          update_option('MAIN_TEL',$_POST['MAIN_TEL']);
        endif;
      endif;

      if(isset($_POST['TEL'])):
        $errmsg['TEL'] = '';
        if(!preg_match('/^[0-9\-]+$/',$_POST['TEL'])) $errmsg['TEL'] = '受付電話番号の入力値が不正です。半角数字、およびハイフン(-)のみ使用できます。';
        if(!$errmsg['TEL']):
          update_option('TEL',$_POST['TEL']);
        endif;
      endif;

      if(isset($_POST['FAX'])):
        $errmsg['FAX'] = '';
        if(!$errmsg['FAX']):
          update_option('FAX',$_POST['FAX']);
        endif;
      endif;

      if(isset($_POST['TEL_TIME'])):
        $errmsg['TEL_TIME'] = '';
        if(!$errmsg['TEL_TIME']):
          update_option('TEL_TIME',$_POST['TEL_TIME']);
        endif;
      endif;

      if(isset($_POST['COPY'])):
        $errmsg['COPY'] = '';
        if(!$errmsg['COPY']):
          update_option('COPY',wp_unslash($_POST['COPY']));
        endif;
      endif;

      if(isset($_POST['ACCESSLOG_TAG'])):
        $errmsg['ACCESSLOG_TAG'] = '';
        if(!$errmsg['ACCESSLOG_TAG']):
          update_option('ACCESSLOG_TAG',wp_unslash($_POST['ACCESSLOG_TAG']));
        endif;
      endif;


      if(isset($_POST['submit'])):
        echo '<p><strong>設定を保存しました。</strong></p>';
      endif;

  endif;# wp_verify_nonce

?>
<h2>サイト設定</h2>
<p>ここで設定した項目内容をphpで呼び出しすることが可能です。</p>
<p>例)会社名呼び出し： get_option('COMPANY_NAME');</p>

<form method="post" action="" enctype="multipart/form-data">

  <table class="form-table settingConfig">

    <tr>
      <th><label for="COMPANY_NAME">会社名[COMPANY_NAME]</label></th>
      <td>
        <input type="text" name="COMPANY_NAME" id="COMPANY_NAME" class="wide" value="<?php echo esc_attr(get_option('COMPANY_NAME')); ?>">
      </td>
    </tr>
    
    <tr>
      <th><label for="COMPANY_NAME">サイトキーワード[COMPANY_KEY]</label></th>
      <td>
        <input type="text" name="COMPANY_KEY" id="COMPANY_KEY" class="wide" value="<?php echo esc_attr(get_option('COMPANY_KEY')); ?>">
      </td>
    </tr>

    <tr>
      <th><label for="CEO_NAME">代表者名[CEO_NAME]</label></th>
      <td>
        <input type="text" name="CEO_NAME" id="CEO_NAME" value="<?php echo esc_attr(get_option('CEO_NAME')); ?>">
      </td>
    </tr>

    <tr>
      <th><label for="POST_ADDRESS">本社住所[POST][ADDRESS]</label></th>
      <td>
        <input type="text" name="POST" id="POST" value="<?php echo esc_attr(get_option('POST')); ?>" placeholder="郵便番号">
        <input type="text" name="ADDRESS" id="ADDRESS" class="wide" value="<?php echo esc_attr(get_option('ADDRESS')); ?>">
      </td>
    </tr>

<!--<tr>
      <th><label for="CEO_NAME">本社MAP URL</label></th>
      <td>
        <input type="text" name="MAP" id="MAP" value="<?php echo esc_attr(get_option('MAP')); ?>">
      </td>
    </tr>-->

    <tr>
      <th><label for="MAIN_TEL">代表電話番号[MAIN_TEL]</label></th>
      <td>
        <input type="text" name="MAIN_TEL" id="MAIN_TEL" value="<?php echo esc_attr(get_option('MAIN_TEL')); ?>">
      </td>
    </tr>
    
    <tr>
      <th><label for="TEL">受付電話番号[TEL]</label></th>
      <td>
        <input type="text" name="TEL" id="TEL" value="<?php echo esc_attr(get_option('TEL')); ?>">
      </td>
    </tr>
    <tr>
      <th><label for="FAX">受付FAX番号[FAX]</label></th>
      <td>
        <input type="text" name="FAX" id="FAX" value="<?php echo esc_attr(get_option('FAX')); ?>">
      </td>
    </tr>
    <tr>
      <th><label for="TEL_TIME">電話受付時間[TEL_TIME]</label></th>
      <td>
        <input type="text" name="TEL_TIME" id="TEL_TIME" value="<?php echo esc_attr(get_option('TEL_TIME')); ?>">
      </td>
    </tr>
    <tr>
      <th><label for="ACCESSLOG_TAG">アナリティクスタグ[ACCESSLOG_TAG]</label></th>
      <td>
        <textarea name="ACCESSLOG_TAG" id="ACCESSLOG_TAG"><?php echo esc_textarea(get_option('ACCESSLOG_TAG')); ?></textarea>
      </td>
    </tr>
  </table>

  <input type="hidden" name="submit" value="1">
  <?php
  wp_nonce_field($action, $nonce);
  submit_button();
  ?>
</form>
<?php

}

/* setting_config実装 CSS調整 */
function custom_admin_style() {
	?><style>
		.settingConfig .wide{
			width: 320px;
		}
		.settingConfig textarea{
			width: 640px;
			height:280px;
		}
	</style><?php
}
add_action( 'admin_head', 'custom_admin_style' );


/**********************************/
/***** メディア系 *****/
/**********************************/

/* メディアページをnoindex */
function my_add_noindex_attachment(){
    if (is_attachment()) {
        echo '<meta name="robots" content="noindex,follow" />';
    }
}
add_action('wp_head', 'my_add_noindex_attachment');

/* メディアデフォルト記述変更　<img src=" " alt=" "  /> */
add_filter( 'image_send_to_editor', 'remove_image_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_image_attribute', 10 );

function remove_image_attribute( $html ){
  $html = preg_replace( '/(width|height)="\d*"\s/', '', $html );
  $html = preg_replace( '/class=[\'"]([^\'"]+)[\'"]/i', '', $html );
  $html = preg_replace( '/title=[\'"]([^\'"]+)[\'"]/i', '', $html );
  $html = preg_replace( '/<a href=".+">/', '', $html );
  $html = preg_replace( '/<\/a>/', '', $html );
  return $html;
}

/* 添付ファイルの表示設定 */
add_action( 'admin_print_styles', 'admin_css_custom' );
function admin_css_custom() {
  echo '<style>.attachment-details label[data-setting="caption"], .attachment-details label[data-setting="description"], div.attachment-display-settings { display: none; }</style>';
}


/**********************************/
/***** 直接記述専用 *****/
/**********************************/

/* 投稿画面で表示されるtextareaでタブ入力可能にする */
add_action( 'admin_footer', 'my_textarea_tab_input' );

function my_textarea_tab_input() {
	if( current_user_can( 'edit_posts' ) === true && stripos( $_SERVER['REQUEST_URI'], 'wp-admin/post.php' ) !== false ) {
		echo <<< SCRIPT
<script>
var textareas = document.getElementsByTagName('textarea');
var count = textareas.length;
for( var i = 0; i < count; i++ ) {
	textareas[i].onkeydown = function(e){
		if( e.keyCode === 9 || e.which === 9 ) {
			e.preventDefault();
			var s = this.selectionStart;
			this.value = this.value.substring( 0, this.selectionStart ) + "\t" + this.value.substring( this.selectionEnd );
			this.selectionEnd = s + 1;
		}
	}
}
</script>
SCRIPT;
	}
}

/* 管理画面で投稿者権限もiframe、script、embed、objectのタグがつかえるようにする */
add_filter('wp_kses_allowed_html','allow_embeddings',10,2);
function allow_embeddings($tags,$context){
	if($context=='post'){
		$tags['iframe']=array(
			'class'=>array(),
			'src'=>array(),
			'width'=>array(),
			'height'=>array(),
			'frameborder'=>array(),
			'scrolling'=>array(),
			'allowtransparency'=>array(),
			'marginheight'=>array(),
			'marginwidth'=>array()
		);
		$tags['script']=array(
			'src'=>array(),
			'type'=>array(),
		);
		$tags['embed']=array(
			'style'=>array(),
			'type'=>array(),
			'id'=>array(),
			'height'=>array(),
			'width'=>array(),
			'src'=>array(),
			'object'=>array(
				'height'=>array(),
				'width'=>array(),
				'param'=>array(
					'name'=>array(),
					'value'=>array()
				)
			)
		);
		$tags['object']=array(
			'height'=>array(),
			'width'=>array(),
			'param'=>array(
				'name'=>array(),
				'value'=>array()
			),
			'embed'=>array(
				'style'=>array(),
				'type'=>array(),
				'id'=>array(),
				'height'=>array(),
				'width'=>array(),
				'src'=>array()
			)
		);
	}
	return($tags);
}

/* 管理画面固定ページのみビジュアルエディタ非表示 */
add_filter('user_can_richedit' , 'my_default_editor');
function my_default_editor( $r ) {
    if ( 'page'== get_current_screen()->id ) {
        return false;
    }else{
        return $r;
    }
}


/* ディスクリプション/キーワード項目追加 */
add_action('admin_menu', 'add_custom_fields');
add_action('save_post', 'save_custom_fields');
 
// 記事ページと固定ページでカスタムフィールドを表示
function add_custom_fields() {
  add_meta_box( 'my_sectionid', 'カスタムフィールド', 'my_custom_fields', 'post');
  add_meta_box( 'my_sectionid', 'カスタムフィールド', 'my_custom_fields', 'page');
}
 
function my_custom_fields() {
  global $post;
  $keywords = get_post_meta($post->ID,'keywords',true);
  $description = get_post_meta($post->ID,'description',true);
   
  echo '<p>キーワード（半角カンマ区切り）<br>';
  echo '<input type="text" name="keywords" value="'.esc_html($keywords).'" size="60" /></p>';
   
  echo '<p>ページの説明（description）160文字以内<br>';
  echo '<input type="text" style="width: 600px;height: 40px;" name="description" value="'.esc_html($description).'" maxlength="160" /></p>';
}
 
// カスタムフィールドの値を保存
function save_custom_fields( $post_id ) {
  if(!empty($_POST['keywords']))
    update_post_meta($post_id, 'keywords', $_POST['keywords'] );
  else delete_post_meta($post_id, 'keywords');
 
  if(!empty($_POST['description']))
    update_post_meta($post_id, 'description', $_POST['description'] );
  else delete_post_meta($post_id, 'description');
}
 
function my_description() {
 
// カスタムフィールドの値を読み込む
$custom = get_post_custom();
if(!empty( $custom['description'][0])) {
  $description = $custom['description'][0];
}
?>
<?php if(is_single()): // 記事ページ ?>
<meta name="description" content="<?php echo $description ?><?php bloginfo('description'); ?>">
<?php elseif(is_page()): // 固定ページ ?>
<meta name="description" content="<?php echo $description ?>">
<?php elseif (is_category()): // カテゴリーページ ?>
<meta name="description" content="<?php single_cat_title(); ?>の記事一覧" />
<?php elseif (is_tag()): // タグページ ?>
<meta name="description" content="<?php single_tag_title("", true); ?>の記事一覧" />
<?php else: // その他ページ ?>
<?php endif; ?>
<?php
}


function my_og_description() {
 
// カスタムフィールドの値を読み込む
$custom = get_post_custom();
if(!empty( $custom['description'][0])) {
  $description = $custom['description'][0];
}
?>
<?php if(is_single()): // 記事ページ ?>
<meta property="og:description" content="<?php echo $description ?><?php bloginfo('description'); ?>">
<?php elseif(is_page()): // 固定ページ ?>
<meta property="og:description" content="<?php echo $description ?>">
<?php elseif (is_category()): // カテゴリーページ ?>
<meta property="og:description" content="<?php single_cat_title(); ?>の記事一覧" />
<?php elseif (is_tag()): // タグページ ?>
<meta property="og:description" content="<?php single_tag_title("", true); ?>の記事一覧" />
<?php else: // その他ページ ?>
<?php endif; ?>
<?php
}