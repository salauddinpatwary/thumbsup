﻿<?php
/*
 Template Name: contact_comp
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

    <!-- #mainCol -->
    <div id="mainCol">

			<div class="subImgTitleBox subImgTitleBoxRecruitform">
				<div class="subImgTitle">応募完了</div>
			</div>
            
			<div class="pankuzu">
			<?php if(function_exists('bcn_display'))
			{
			bcn_display();
			}?>
			</div>
	        
			<div class="subContentBox">
			
				<div class="recPostsFrm">
				送信しました。<br />
				この度はお問合せメールをお送りいただきありがとうございます。<br />
				後ほど、担当よりご連絡をさせていただきます。<br />
				今しばらくお待ちくださいますようよろしくお願い申し上げます。<br /><br />

				なお、しばらくたっても弊社より返信、返答がない場合は、<br />
				お客様によりご入力いただいたメールアドレスに誤りがある場合がございます。<br />
				その際は、お手数ですが再度送信いただくか、<br />
				お電話までご連絡いただけますと幸いです。<br /><br />

				何かご不明な点等ございましたら、お気軽にお問合せ下さい。
				<?php //お問い合わせフォームショートコード 対応させる応募フォームのWPform番号に変えて下さい
								echo do_shortcode('[mwform_formkey key="10"]'); ?>
				</div>

		</div>


	<!-- /#mainCol -->
	
<?php get_template_part('sidenavi'); ?>
<?php get_template_part('subfooter'); ?>