    <!-- footer -->
    <footer class="l-footer">
      <section class="l-footer-wrap">

        <div class="l-footer-info">
          <div class="l-footer-logo"><span class="l-footer-logo-wrap u-en">Thumbs Up</span></div>
          <span class="l-footer-address"><?php echo get_option('POST'); ?> <?php echo get_option('ADDRESS'); ?></span>
          <small class="l-footer-copy">&copy; 2018 Thumbs Up Corporation.</small>
        </div>

        <div class="l-footer-nav">
          <ul class="l-footer-nav-list">
            <li class="l-footer-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/trouble/' )); ?>" class="c-menu-up__item">マンション経営でお悩みの方へ</a></div></li>
            <li class="l-footer-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/answer/' )); ?>" class="c-menu-up__item">資産価値・収益の最大化</a></div></li>
            <li class="l-footer-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/works/' )); ?>" class="c-menu-up__item">施工事例</a></div></li>
            <li class="l-footer-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/voice/' )); ?>" class="c-menu-up__item">大家様の声</a></div></li>
          </ul>
          <div class="l-footer-nav-btn">
            <div class="l-footer-nav-btn__item c-btn-small__border--white"><a href="<?php echo esc_url(home_url( '/contact/' )); ?>"><span class="c-btn-small__border--white-wrap">お問い合わせ</span></a></div>
            <div class="l-footer-nav-btn__item c-btn-small__border--white"><a href="<?php echo esc_url(home_url( '/assessment/' )); ?>"><span class="c-btn-small__border--white-wrap">査定依頼</span></a></div>
          </div>
        </div>

      </section>
    </footer>
    <!-- /footer -->
  </div>

<!-- [import script] -->

<script src="<?php echo home_url( '/common/js/vendor.js' ); ?>"></script>
<script src="<?php echo home_url( '/common/js/common.js' ); ?>"></script>
<?php if (is_front_page()): //トップ ?>
<script src="<?php echo home_url( '/common/js/index.js' ); ?>"></script>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>
