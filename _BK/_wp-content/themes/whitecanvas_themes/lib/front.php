<?php

/**********************************/
/***** フロント系 *****/
/**********************************/

/* ヘッダーの不要記述削除対応(EditURI,wlwmanifest,WordPressのバージョン情報) */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');

/* ヘッダーの不要記述削除対応(絵文字) */
function disable_emoji() {
     remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
     remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
     remove_action( 'wp_print_styles', 'print_emoji_styles' );
     remove_action( 'admin_print_styles', 'print_emoji_styles' );    
     remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
     remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );    
     remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );

/* ヘッダーの不要記述削除対応(絵文字使わないのでdns-prefetchも) */
remove_action('wp_head','wp_resource_hints',2);

/* ヘッダーの不要記述削除対応(フィードリンク) */
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);

/* ヘッダーの不要記述削除対応(短縮URL) */
remove_action('wp_head', 'wp_shortlink_wp_head');

/* ヘッダーの不要記述削除対応(Embed系) */
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');

/* ヘッダーの不要記述削除対応(カノニカル) */
remove_action('wp_head', 'wp_shortlink_wp_head');

/* 404対応 */
function is404_redirect_home() {
	if( is_404() ){
		wp_redirect(home_url());
		exit();
	}
}
add_action( 'template_redirect', 'is404_redirect_home' );


/* 投稿者アーカイブにアクセスさせない */
add_filter( 'author_rewrite_rules', '__return_empty_array' );
function disable_author_archive() {
if( $_GET['author'] || preg_match('#/author/.+#', $_SERVER['REQUEST_URI']) ){
wp_redirect( home_url( '/notfound/' ) );
exit;
}
}
add_action('init', 'disable_author_archive');

/* contact MW WP From 設定する際は数字を変更して使用*/
//add_filter( 'mwform_choices_mw-wp-form-00', 'add_products', 10, 2 );

/* 年月別アーカイブを年別へ変更 */
add_filter( 'widget_archives_args', 'hook_widget_archives_args' );
function hook_widget_archives_args( $args ) {
    
    // デフォルトの月別(type=monthly)から年別のアーカイブに変更
    $args['type'] = 'yearly';

    // 最大出力件数を5件までに制限
    $args['limit'] = 5;
    
    return $args;
}

//PCとスマホの記事中表示分岐用ショートコード
function contents_for_pc( $atts, $content = null ) {
    if(wp_is_mobile()) {  
            return '';   
     } else {
            return '' . $content . '';
     }
  }
  function contents_for_sp( $atts, $content = null ) {
    if(wp_is_mobile()) {  
            return '' . $content . '';   
     } else {
            return '';
     }
  }
    add_shortcode('pc', 'contents_for_pc');
    add_shortcode('sp', 'contents_for_sp'); 
  
  //IEとそれ以外での記事中表示分岐用ショートコード
  function contents_for_ie( $atts, $content = null ) {
    global $is_IE;
    if($is_IE) {
      return '' . $content . '';
     } else {
       return '';   
    }
  }
  function contents_for_not_ie( $atts, $content = null ) {
    global $is_IE;
    if($is_IE) {
      return '';
     } else {
       return '' . $content . '';
    }
  }
  
  add_shortcode('ie', 'contents_for_ie');
  add_shortcode('not_ie', 'contents_for_not_ie');