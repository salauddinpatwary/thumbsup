<?php get_header(); ?>

						
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php
								/*
								 * Ah, post formats. Nature's greatest mystery (aside from the sloth).
								 *
								 * So this function will bring in the needed template file depending on what the post
								 * format is. The different post formats are located in the post-formats folder.
								 *
								 *
								 * REMEMBER TO ALWAYS HAVE A DEFAULT ONE NAMED "format.php" FOR POSTS THAT AREN'T
								 * A SPECIFIC POST FORMAT.
								 *
								 * If you want to remove post formats, just delete the post-formats folder and
								 * replace the function below with the contents of the "format.php" file.
								*/
								get_template_part( 'post-formats/format-work', get_post_format() );
							?>
						<?php endwhile; ?>
						<?php else : ?>
						<?php endif; ?>

<section class="c-section p-work-relation c-base-m-t--minus">
		<div class="c-section-1280">

			<div class="c-header u-m-b-50" data-scroll="toggle(.c-fadein, .c-fadeout) once">
				<h2 class="c-title-regular">オススメ事例</h2>
				<p class="c-read">立地からニーズを読み取り、マンション経営を最大限に活かせるリフォームをご提案いたします。空室物件やマンション経営にお悩みの方はぜひサムズアップの施工事例をご覧ください。内装や細部の造形にもこだわり抜いた特別なデザインをどうぞ。</p>
			</div>

			<ul class="c-box3-list u-clearfix">
<?php
if(has_category() ) {
	$cats =get_the_category();
	$catkwds = array();
	foreach($cats as $cat){
		$catkwds[] = $cat->term_id;
	}
}
$args = array(
	'post_type' => 'work',
	'posts_per_page' => '3',
	'post__not_in' =>array( $post->ID ),
	'category__in' => $catkwds,
	'orderby' => 'rand'
);
$my_query = new WP_Query( $args );
while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

				<li class="c-box3-list__item" data-scroll="toggle(.c-fadein, .c-fadeout) once">
					<a href="<?php the_permalink(); ?>">
						<div class="c-box3-list__image c-work-list__image">
							<img class="c-box3-list__image" src="<?php
//フィールド名「image_test」のフルサイズ画像の情報を取得
$image = wp_get_attachment_image_src(get_field('photo_main'), 'full');
if( !empty($image) ) { //画像があれば表示
     echo $image[0];
 } else {
 	echo "{$url}/common/img/work/img_work03_01.jpg";
 }
?>" alt="">
						</div>
						<h3 class="c-box3-list__title"><?php the_title(); ?></h3>
					</a>
				</li>

<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
			</ul>
		</div>

</section>

<?php get_footer(); ?>

