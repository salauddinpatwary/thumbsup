<?php
  /*
  施工事例情報用format
  */

  //フィールド名一覧
  $works_title = get_field('works_title');//事例タイトル
  $result_txt01 = get_field('result_txt01');//結果コメント1
  $result_txt02 = get_field('result_txt02');//結果コメント2
  $photo_main = get_field('photo_main');//メイン画像
  $area = get_field('area');//都道府県
  $construction = get_field('construction');//構造
  $floor = get_field('floor');//間取り
  $occupied_area = get_field('occupied_area');//専有面積
  $garary_photo01_before = get_field('garary_photo01_before');//ギャラリー1_施工前写真
  $garary_photo01_after = get_field('garary_photo01_after');//ギャラリー1_施工後写真
  $garary_txt01 = get_field('garary_txt01');//ギャラリー1コメント
  $garary_photo02_before = get_field('garary_photo02_before');//ギャラリー2_施工前写真
  $garary_photo02_after = get_field('garary_photo02_after');//ギャラリー2_施工後写真
  $garary_txt02 = get_field('garary_txt02');//ギャラリー2コメント
  $garary_photo03_before = get_field('garary_photo03_before');//ギャラリー3_施工前写真
  $garary_photo03_after = get_field('garary_photo03_after');//ギャラリー3_施工後写真
  $garary_txt03 = get_field('garary_txt03');//ギャラリー3コメント
  $garary_photo04_before = get_field('garary_photo04_before');//ギャラリー4_施工前写真
  $garary_photo04_after = get_field('garary_photo04_after');//ギャラリー4_施工後写真
  $garary_txt04 = get_field('garary_txt04');//ギャラリー4コメント
  $garary_photo05_before = get_field('garary_photo05_before');//ギャラリー5_施工前写真
  $garary_photo05_after = get_field('garary_photo05_after');//ギャラリー5_施工後写真
  $garary_txt05 = get_field('garary_txt05');//ギャラリー5コメント

  $imgurl = wp_get_attachment_image_src($photo_main, 'full');
  
?>

<section class="p-work-result">
  <div class="p-work-result-wrap">
    <?php echo $result_txt01; ?>
    <?php echo $result_txt02; ?>
  </div>
</section>

<section class="c-sub-kv">
  <div class="c-sub-kv-wrap" style="background-image:url('<?php echo $imgurl[0]; ?>');background-size:cover;">
  </div>
</section>

<section class="p-work-header">
  <div class="c-section-1280">

      <div class="p-work-header-wrap u-clearfix">

        <div class="p-work-title">
          <div class="p-work-title-wrap">
            <span class="p-work-title__sub"><?php echo $area; ?></span>
            <h1 class="p-work-title__text"><?php echo  $works_title; ?></h1>
          </div>
        </div>

        <ul class="p-work-detail">
<?php
if($construction): ?>
          <li class="p-work-detail__item">構造 / <?php echo $construction; ?></li>
<?php
endif;
if($floor): ?>
          <li class="p-work-detail__item">間取りタイプ / <?php echo $floor; ?></li>
<?php
endif;
if($occupied_area): ?>
          <li class="p-work-detail__item">専有面積 / <?php echo $occupied_area ?></li>
<?php endif; ?>

        </ul>
      </div>
  </div>
</section>

<?php
$garary_photo01_before = wp_get_attachment_image_src($garary_photo01_before, 'full');
$garary_photo01_after = wp_get_attachment_image_src($garary_photo01_after, 'full');
?>

<section class="p-work-pick" data-scroll="toggle(.c-fadein, .c-fadeout) once">
  
  <div class="p-work-slider c-base-m-t--minus">
    <div class="c-section">
      <div class="p-work-slider-wrap ba-slider">
        <img src="<?php echo $garary_photo01_after[0]; ?>">
        <div class="resize">
          <img src="<?php echo $garary_photo01_before[0]; ?>">
        </div>
        <span class="handle"></span>
      </div>
    </div>
    <span class="p-work-pick__before u-en">Before</span>
    <span class="p-work-pick__after u-en">After</span>
  </div>

  <div class="p-work-pick-description c-base-p-b">
    <div class="c-section p-work-pick-description-wrap">
      <div class="c-section-1280">
        <p class="p-work-comment"><?php echo $garary_txt01 ?></p>
      </div>

      <div class="c-next-read">
        <div class="c-next-read-wrap">
          <span class="c-next-read__text">写真ギャラリー</span>
        </div>
      </div>
    </div>
  </div>
  
</section>

<section class="c-section p-work-gallery">

    <div class="p-work-gallery__item c-base-m-b--half" data-scroll="toggle(.c-fadein, .c-fadeout) once">
      <div class="p-work-gallery__header">
        <span class="p-work-gallery__title">リフォーム前</span>
      </div>
      <div class="p-work-gallery__header">
        <span class="p-work-gallery__title">リフォーム後</span>
      </div>
    </div>

<?php
$garary_photo02_before = wp_get_attachment_image_src($garary_photo02_before, 'full');
$garary_photo02_after = wp_get_attachment_image_src($garary_photo02_after, 'full');
if($garary_photo02_after) { ?>
    <div class="p-work-gallery__item c-base-m-b">
      <div class="p-work-gallery__item-images c-base-m-b--half" data-scroll="toggle(.c-fadein, .c-fadeout) once">

<?php
if($garary_photo02_before) { ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo02_before[0]; ?>"></div>
<?php } ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo02_after[0]; ?>"></div>
      </div>
<?php if($garary_txt02) { ?>
      <div class="c-section-1280">
        <p class="p-work-comment" data-scroll="toggle(.c-fadein, .c-fadeout) once"><?php echo $garary_txt02; ?></p>
      </div>
<?php } ?>
    </div>
<?php } ?>


<?php
$garary_photo03_before = wp_get_attachment_image_src($garary_photo03_before, 'full');
$garary_photo03_after = wp_get_attachment_image_src($garary_photo03_after, 'full');
if($garary_photo03_after) { ?>
    <div class="p-work-gallery__item c-base-m-b">
      <div class="p-work-gallery__item-images c-base-m-b--half" data-scroll="toggle(.c-fadein, .c-fadeout) once">

<?php
if($garary_photo03_before) { ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo03_before[0]; ?>"></div>
<?php } ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo03_after[0]; ?>"></div>
      </div>
<?php if($garary_txt03) { ?>
      <div class="c-section-1280">
        <p class="p-work-comment" data-scroll="toggle(.c-fadein, .c-fadeout) once"><?php echo $garary_txt03; ?></p>
      </div>
<?php } ?>
    </div>
<?php } ?>

<?php
$garary_photo04_before = wp_get_attachment_image_src($garary_photo04_before, 'full');
$garary_photo04_after = wp_get_attachment_image_src($garary_photo04_after, 'full');
if($garary_photo04_after) { ?>
  <div class="p-work-gallery__item c-base-m-b">
      <div class="p-work-gallery__item-images c-base-m-b--half" data-scroll="toggle(.c-fadein, .c-fadeout) once">

<?php
if($garary_photo04_before) { ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo04_before[0]; ?>"></div>
<?php } ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo04_after[0]; ?>"></div>
      </div>
<?php if($garary_txt04) { ?>
      <div class="c-section-1280">
        <p class="p-work-comment" data-scroll="toggle(.c-fadein, .c-fadeout) once"><?php echo $garary_txt04; ?></p>
      </div>
<?php } ?>
    </div>
<?php } ?>

<?php
$garary_photo05_before = wp_get_attachment_image_src($garary_photo05_before, 'full');
$garary_photo05_after = wp_get_attachment_image_src($garary_photo05_after, 'full');
if($garary_photo05_after) { ?>
    <div class="p-work-gallery__item c-base-m-b">
      <div class="p-work-gallery__item-images c-base-m-b--half" data-scroll="toggle(.c-fadein, .c-fadeout) once">

<?php
if($garary_photo05_before) { ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo05_before[0]; ?>"></div>
<?php } ?>
        <div class="p-work-box2"><img class="p-work-box2__image" src="<?php echo $garary_photo05_after[0]; ?>"></div>
      </div>
<?php if($garary_txt05) { ?>
      <div class="c-section-1280">
        <p class="p-work-comment" data-scroll="toggle(.c-fadein, .c-fadeout) once"><?php echo $garary_txt05; ?></p>
      </div>
<?php } ?>
    </div>
<?php } ?>

</section>