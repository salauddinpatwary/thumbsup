
              <?php
                /*
                 * This is the default post format.
                 *
                 * So basically this is a regular post. if you don't want to use post formats,
                 * you can just copy ths stuff in here and replace the post format thing in
                 * single.php.
                 *
                 * The other formats are SUPER basic so you can style them as you like.
                 *
                 * Again, If you want to remove post formats, just delete the post-formats
                 * folder and replace the function below with the contents of the "format.php" file.
                */
              ?>
              <section class="c-section">
                <div class="c-section-1280">
                  <h2 class="c-title-h2--border">新着情報</h2>
                </div>
              </section>


              <article class="c-section p-news-detail__content" role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
                <div class="c-section-555">
                  <div class="p-news-detail__date u-clearfix">

                  <?php printf(' %1$s %2$s',
                    /* the time the post was published */
                    '<time class="c-news-date" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' .get_the_time('Y.m.d') . '</time>',
                    /* the author of the post */
                    '<span class="c-news-category" itemprop="author" itemscope itemptype="http://schema.org/Person">' . get_the_category_list(', ') . '</span>'
                  ); ?>
                  </div>

                  <h1 class="p-news-detail__title" itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>

                  


                  <section class="p-news-detail__text c-base-m-b" itemprop="articleBody">
                  <?php
                    // the content (pretty self explanatory huh)
                    the_content();

                    /*
                    * Link Pages is used in case you have posts that are set to break into
                    * multiple pages. You can remove this if you don't plan on doing that.
                    *
                    * Also, breaking content up into multiple pages is a horrible experience,
                    * so don't do it. While there are SOME edge cases where this is useful, it's
                    * mostly used for people to get more ad views. It's up to you but if you want
                    * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
                    *
                    * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
                    *
                    */
                    wp_link_pages( array(
                      'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
                      'after'       => '</div>',
                      'link_before' => '<span>',
                      'link_after'  => '</span>',
                    ) );
                  ?>
                  </section> <?php // end article section ?>

                  <div class="c-btn-regular--black c-base-m-b">
                    <a href="../">
                      <div class="c-btn-regular--black-wrap">
                        <span class="c-btn-regular--black__text">新着情報一覧に戻る</span>
                        <div class="c-btn-box-arrow--left">
                          <div class="c-btn-box-arrow--left-wrap"></div>
                        </div>
                      </div>
                    </a>
                  </div>

                </div>

                <?php //comments_template(); ?>

              </article> <?php // end article ?>
