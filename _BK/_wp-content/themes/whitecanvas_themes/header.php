<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php // ページタイトル設定 ?>
<?php if(is_front_page()): //トップ ?>
<title><?php bloginfo('name'); ?></title>
<?php elseif (is_home()): //新着情報記事一覧 ?>
<title>新着情報一覧｜<?php bloginfo('name'); ?></title>
<?php elseif( is_archive() ) : //新着情報記事アーカイブ ?>
<title>過去の情報一覧｜<?php bloginfo('name'); ?></title>
<?php elseif( is_category() ) : //新着情報記事カテゴリ ?>
<title>「<?php echo get_cat_name($cat->term_id); ?>」カテゴリの情報一覧｜<?php bloginfo('name'); ?></title>
<?php else: //デフォルト ?>
<title><?php the_title(); ?>｜<?php bloginfo('name'); ?></title>
<?php endif; ?>

<?php // ページメタ設定 ?>
<?php if(is_front_page()): // トップ ?>
<meta name="description" content="<?php bloginfo('description'); ?>" />
<?php elseif (is_home()): //新着情報記事一覧 ?>
<meta name="description" content="新着情報をご紹介します。<?php bloginfo('description'); ?>" />
<?php elseif( is_archive() ) : //新着情報記事アーカイブ ?>
<meta name="description" content="過去のニュース情報をご紹介します。<?php bloginfo('description'); ?>" />
<?php elseif( is_category() ) : //新着情報記事カテゴリ ?>
<meta name="description" content="「<?php echo get_cat_name($cat->term_id); ?>」カテゴリの情報をご紹介します。<?php bloginfo('description'); ?>" />
<?php else: // デフォルト ?>
<?php my_description(); ?>
<?php endif; ?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">

<meta property="og:type" content="<?php if($_SERVER["REQUEST_URI"] == "/renovation/"){echo "website";}else{echo "article";}?>" />
<meta property="og:site_name" content="サムズアップ" />
<?php // og設定 ?>
<?php if (is_front_page()): //トップ ?>
<meta property="og:title" content="サムズアップ" />
<?php elseif (is_home()): //新着情報記事一覧 ?>
<meta property="og:title" content="新着情報一覧｜サムズアップ" />
<?php elseif( is_archive() ) : //新着情報記事アーカイブ ?>
<meta property="og:title" content="過去の情報一覧｜サムズアップ" />
<?php elseif( is_category() ) : //新着情報記事カテゴリ 
$cat = get_the_category();
$cat = $cat[0];
?>
<meta property="og:title" content="「<?php echo get_cat_name($cat->term_id); ?>」カテゴリのニュース一覧｜サムズアップ" />
<?php else: //デフォルト ?>
<meta property="og:title" content="<?php the_title(); ?>｜サムズアップ" />
<?php endif; ?>
<?php if(is_front_page()): // トップ ?>
<meta property="og:description" content="<?php bloginfo('description'); ?>" />
<?php elseif (is_home()): //新着情報記事一覧 ?>
<meta property="og:description" content="新着情報をご紹介します。<?php bloginfo('description'); ?>" />
<?php elseif( is_archive() ) : //新着情報記事アーカイブ ?>
<meta property="og:description" content="過去のニュース情報をご紹介します。<?php bloginfo('description'); ?>" />
<?php elseif( is_category() ) : //新着情報記事カテゴリ ?>
<meta property="og:description" content="「<?php echo get_cat_name($cat->term_id); ?>」カテゴリの情報をご紹介します。<?php bloginfo('description'); ?>" />
<?php else: // デフォルト ?>
<?php my_og_description(); ?>
<?php endif; ?>
<meta property="og:image" content="<?php echo esc_url(home_url( '/common/img/og_image.png' )); ?>" />
<meta property="og:url" content="<? echo (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" />

<?php // Twitter用 ?>
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:domain" content="<?php echo home_url( '/' ); ?>">
<link rel="shortcut icon" href="<?php echo esc_url(home_url( '/common/img/favicon.ico' )); ?>" type="image/vnd.microsoft.icon">

<?php echo get_option('ACCESSLOG_TAG'); ?>

<!-- .css stylesheet -->
<link href="https://fonts.googleapis.com/css?family=Reem+Kufi" rel="stylesheet">
<link href="<?php echo esc_url(home_url( '/common/css/style.css' )); ?>" rel="stylesheet" media="all" type="text/css">

<?php wp_deregister_script('jquery'); ?>
<?php wp_head(); ?>
		
</head>

<body class="c-body">
  <div class="l-wrap">

    <header class="l-header">
<?php
  if($_SERVER["REQUEST_URI"] == "/renovation/") {
?>
      <h1><a href="<?=home_url( '/' );?>" class="l-header-logo u-en">Thumbs Up</a></h1>
<?php
    } else {
?>
      <span><a href="<?=home_url( '/' );?>" class="l-header-logo u-en">Thumbs Up</a></span>
<?php } ?>
      <nav class="l-header-nav">
        <ul class="l-header-nav-list">
          <li class="l-header-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/trouble/' )); ?>" class="c-menu-up__item">マンション経営でお悩みの方へ</a></div></li>
          <li class="l-header-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/answer/' )); ?>" class="c-menu-up__item">資産価値・収益の最大化</a></div></li>
          <li class="l-header-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/works/' )); ?>" class="c-menu-up__item">施工事例</a></div></li>
          <li class="l-header-nav-list__item"><div class="c-menu-up"><a href="<?php echo esc_url(home_url( '/voice/' )); ?>" class="c-menu-up__item">大家様の声</a></div></li>
        </ul>
      </nav>
      <div class="l-header-btn c-btn-small__border--gray"><a href="<?php echo esc_url(home_url( '/contact/' )); ?>"><span class="c-btn-small__border--gray-wrap u-en">Contact</span></a></div>
    </header>

    <div class="c-toggle">
      <div class="c-toggle-wrap">
        <span class="c-toggle__line"></span>
        <span class="c-toggle__line"></span>
        <span class="c-toggle__line"></span>
      </div>
    </div>

    <nav class="l-nav">
      <div class="l-nav__contents">
        <span class="l-nav__logo"><a href="<?php echo esc_url(home_url( '/' )); ?>" class="u-en">Thumbs Up</a></span>

        <div class="l-nav__title">
              <span class="u-en">Menu</span>
        </div>
        
        <div class="l-nav-list">
          <ul class="l-nav-list-wrap u-clearfix">
            <li class="l-nav-list__item"><a href="<?php echo esc_url(home_url( '/trouble/' )); ?>">マンション経営でお悩みの方へ</a></li>
            <li class="l-nav-list__item"><a href="<?php echo esc_url(home_url( '/answer/' )); ?>">資産価値、収益の最大化</a></li>
            <li class="l-nav-list__item"><a href="<?php echo esc_url(home_url( '/works/' )); ?>">施工事例</a></li>
            <li class="l-nav-list__item"><a href="<?php echo esc_url(home_url( '/voice/' )); ?>">大家様の声</a></li>
          </ul>
        </div>

        <div class="l-nav-btn">
          <div class="l-nav-btn-wrap">
            <div class="l-nav-btn__item c-btn-small__border--white"><a href="<?php echo esc_url(home_url( '/contact/' )); ?>"><span class="c-btn-small__border--white-wrap">お問い合わせ</span></a></div>
            <div class="l-nav-btn__item c-btn-small__border--white"><a href="<?php echo esc_url(home_url( '/assessment/' )); ?>"><span class="c-btn-small__border--white-wrap">査定依頼</span></a></div>
          </div>
        </div>

      </div>
    </nav>

    <div class="p-floating-nav">
      <ul class="p-floating-nav-list">
        <li class="p-floating-nav-list__item"><a href="<?php echo esc_url(home_url( '/assessment/' )); ?>"><img src="<?php echo esc_url(home_url( '/common/img/btn_side_assessment.jpg' )); ?>" alt="査定依頼"></a></li>
        <li class="p-floating-nav-list__item"><a href="<?php echo esc_url(home_url( '/contact/' )); ?>"><img src="<?php echo esc_url(home_url( '/common/img/btn_side_contact.jpg' )); ?>" alt="お問い合わせ"></a></li>
      </ul>
    </div>
