<?php get_header(); ?>
            <section class="c-section">
                <div class="c-section-1280">
									<h1 class="c-title-h2--border c-base-m-b">新着情報</h1>
									<div class="c-header p-news-header c-base-m-b--half">
										<h3 class="c-title-regular">
											<?php
											the_archive_title();
											?>
										</h3>
                  	<?php get_sidebar(); ?>
									</div>
                  <div class="c-news c-base-m-b p-news" data-scroll="toggle(.c-fadein, .c-fadeout) once">
							

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

										<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
											

											<article id="post-<?php the_ID(); ?>" <?php post_class( 'c-news-item-wrap p-news-item-wrap' ); ?> role="article">
												<span class="c-news-date"><?php the_time('Y.m.d'); ?></span>
												<span class="c-news-category"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></span>
												<h2 class="c-news-title"><?php the_title_attribute(); ?></h2>
												<div class="c-news-btn c-btn-arrow">
													<div class="c-btn-arrow-wrap"></div>
												</div>
											</article>

										</a>
										<?php endwhile; ?>
									</div>
									<?php bones_page_navi(); ?>
              	</div>
              </section>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>
	
<?php get_footer(); ?>
