<?php get_header(); ?>


<?php
  /*
  施工事例情報アーカイブ用
  */

  //フィールド名一覧
  $works_title = get_field('works_title');//事例タイトル
  $result_txt01 = get_field('result_txt01');//結果コメント1
  $result_txt02 = get_field('result_txt02');//結果コメント2
  $photo_main = get_field('photo_main');//メイン画像
  $area = get_field('area');//都道府県
  $construction = get_field('construction');//構造
  $floor = get_field('floor');//間取り
  $occupied_area = get_field('occupied_area');//専有面積
  $garary_photo01_before = get_field('garary_photo01_before');//ギャラリー1_施工前写真
  $garary_photo01_after = get_field('garary_photo01_after');//ギャラリー1_施工後写真
  $garary_txt01 = get_field('garary_txt01');//ギャラリー1コメント
  $garary_photo02_before = get_field('garary_photo02_before');//ギャラリー2_施工前写真
  $garary_photo02_after = get_field('garary_photo02_after');//ギャラリー2_施工後写真
  $garary_txt02 = get_field('garary_txt02');//ギャラリー2コメント
  
?>
		
	<section class="mainCol clearfix">
	<p>アーカイブページだよ！ここに記入すると良いのです。ulタグ以下は記事ループ★</p>
		<ul>
		<?php
		//絞り込みした記事を表示するループ
		$paged = get_query_var('paged') ? get_query_var('paged') : 1 ;
		$args = array(
		'post_type' => 'work',//投稿タイプを設定
		'paged' => $paged,
		);
		$wp_query = new WP_Query();
		$wp_query->query( $args );
		while ( $wp_query->have_posts() ) : $wp_query->the_post();
		?>
		
			<li>
					
					<?php
					$imgurl = wp_get_attachment_url(get_post_meta($wp_query->post->ID,'photo_main',true));
					if($imgurl){ ?>
					<p class="marginBottom20"><img style="width:100%;" src="<?php echo wp_get_attachment_url(get_post_meta($wp_query->post->ID,'photo_main',true));?>" alt=""></p>
					<?php } ?>
					
					<p class="marginBottom20"><a href="<?php the_permalink() ?>"><?php echo get_post_meta( $wp_query->post->ID , 'works_title' , true ); ?></a></p>
					
			</li>

		<?php endwhile; ?>
		</ul>
		
		<?php bones_page_navi(); ?>

		<?php wp_reset_query(); ?>
		
		</section>
				
	</article>
	<!-- /#article -->
	
<?php get_footer(); ?>