

<!doctype html>
<html amp lang="en">

  <head>

    <title>{keyword}</title>
    <style type="text/css">body {
  margin:0px;
  padding:0px;
  background:#fefefe;
  font-family: Arial, Verdana, sans-serif;
  position: relative;
}

a:link, a:visited {
  color: #000080
}

a:hover {
  color: blue;
}

h1 {
  padding: 0px;
  margin: 0px;
  font-family: Verdana, sans-serif;
  font-weight: normal;
  text-align: left;
}

.clearfix {
  clear:both;
  height:0;
  overflow:hidden;
  //height:1px;
}

.center {
  text-align:center;
}

   /***ADS Styling BEGIN***/
.gLinkUnit {
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-style: italic;
  margin-left: 15px;
  text-indent: -15px;
}

.gLinkUnitKeyword {
  font-weight: bold;
}

.gLinkUnitInline {

}

.gLinkUnitInline .gLinkUnitCaption, .gLinkUnitInline .gLinkUnitKeyword {
  display: inline;
}

.gLinkUnitCaption a {
  text-decoration: none;
  color: black;
  font-family: Arial, sans-serif;
}

.gLinkUnitInline .gLinkUnitKeyword a {
  padding: .2em .5em;
}

.gLinkUnitKeyword a:hover {
  color: #0000FF;
}

.gLinkUnitBlock .gLinkUnitKeyword {
  padding: 2px 0 0 10px;
  text-indent: -5px;
}

.gLinkUnitBlock{}

.gLinkUnitInline{}

.gLinkUnitInline span {
  font-family: Arial,sans-serif;
}


.InsideTextAds {
  margin-bottom: 1.4em;
  font-family: Arial, sans-serif;
}

.InsideTextAds .google_link_cell {
/*  margin-bottom: 5px;*/
}

.InsideTextAds .google_link_cell a,
.InsideTextAds .google_link_cell a:hover {
  font-size:14px;
  color:#000000;
  text-decoration:none;
  font-style: italic;
  font-family: Arial, sans-serif;
}

.InsideTextAds a:hover {
  background-color: transparent !important;
}

.InsideTextAds div.hl0{ background-color: #e5e5e5;}
.InsideTextAds div.hl1{ background-color: #d9d9d9;}

.InsideTextAds .main_link a:hover {
  color:#0000FF;
}

.InsideTextAds .main_link {
  padding: 0px 7px 0px 0px;
  display: inline;
}

.InsideTextAds .main_link a {
  font-size: 14px;
  font-weight: bold;
  font-style: italic;
  text-decoration: underline;
}

.InsideTextAds .description_link {
  color:#000000;
  text-decoration:none;
  display: inline;
  font-style: italic;
  line-height: 150%;
}

.InsideTextAds .description_link {
  display: inline;
  padding-right: 12px;
}

.InsideTextAds .visible_url {
  display: inline;
  font-style: italic;
}

.InsideTextAds .visible_url a {
  color:#0000080;
}

table.ads_table td.ad_links div.visible_url a:hover,
.InsideTextAds .visible_url a:hover {
  color:#0000ff;
}
   /***ADS Styling END***/

div.HeaderContainer {
	padding: 3px 5px;
	border-bottom: solid 4px #45A4D0;
	color: #fefefe;
	background: none repeat scroll 0 0 #24364F;
}

div.HeaderContainer .gLinkUnit,
div.HeaderContainer .gLinkUnitCaption a,
div.HeaderContainer .gLinkUnitKeyword a {
  color: #fefefe;
}

div.HeaderContainer .gLinkUnitCaption a {
  color: #bababa
}


div.ContentContainer {
  padding: 10px;
  background:  url("/res/mobile-1ad-per-pv-js/img/merged_bgs.gif") repeat-x scroll left -273px #CCCCCC;
}

div.FooterContainer {
  padding-top: 10px;
  background-color: #cccccc;
}

div.FooterContainer .Copyright {
  padding: 10px 0px;
  background-color: #000000;
  color: #ffffff;
  text-align: center;
}

div.FooterContainer .Navigation {
  padding: 15px 0px;
  text-align: center;
}

div.FooterContainer .Navigation br {line-height: 2;}
div.FooterContainer .Navigation a.item {margin-left: 8px;}

span.mContent li,
div.ContentContainer p,
div.ContentContainer table.chart {
}

span.mContent a:hover {
  color:#00F;
}

h2.SubSectionTitle {
  padding: 5px 0px;
  margin: 0px;
  background-color: #666666;
  text-align: center;
  font-size: 200%;
  font-style: oblique;
  font-weight: normal;
  color: #fefefe;
}

div.LinkBlock {
}

div.LinkBlock a {}

div.LinkBlockSeparator {
  height: 15px;
  overflow: hidden;
}

.AlignCenter {
  text-align: center;
}

#lightbox-container-image-box {
	background-color: #24364C!important;
}

#lightbox-container-image-data-box {
	background-color: #24364C!important;
}

#lightbox-image-details{
	text-align:center!important;
	width:90%!important;
}

#lightbox-container-image-data{
	padding:0px!important;
	color: #fff!important;
}

#lightbox-container-image-data #lightbox-image-data-btnClose{
	float:right;
}

#lightbox-container-image-bottomNav{
	background-color: #24364C!important;
	padding-bottom:10px!important;
/*	height:200px;*/
}


div.container {
  width: 100%;
}

ul.ulCategories {
  padding:0;
  margin:0;
  margin-left: 13px;
}
ul.ulCategories li{
  padding-left: 20px;
  list-style-type: none;
}
ul.ulCategories li a{
  margin-left:-10px;
}

.allc {display: table; width: 100%; height: 100%; font-size: 0px;}
.allc > .allc {display: table-cell; width: 100%; height: 100%; vertical-align: middle; text-align: center;}

.overlay {position: absolute; width: 100%; height: 100%; z-index: 1; opacity: 0.8; background-color: #333;}
.lightbox {position: relative; width: 85%; height: 100%; margin: 0px auto; z-index: 2;}
.lightbox .cover {display: inline-block; border: 10px solid #24364C; background-color: #24364C;}
.lightbox p {margin: 10px 0px 0px 0px; padding: 0px; font-size: 12pt; line-height: 1.4em; color: #fff;}

.gad-cp {font-size: 0px; line-height: 0px; display: inline;}
.gad-offset {display: block; margin: 20px 0px;}
.gad-unit {display: block; padding: 0px 10px; position: absolute; bottom: 0px; width: 100%; box-sizing: border-box;}
</style><style type="text/css">h2.ArticleTitle, h1.ArticleTitle {
  margin-left: 15px;
  text-indent: -15px;
  font-size: 150%;
  font-style: oblique;
	background: none repeat scroll 0 0 #24364F;
}

div.dPostsDiv {
  margin-top: 20px;
  padding: 0px 5px;
}

div.dPostsDiv .InsideTextAds,
div.dPostsDiv .InsideTextAds .description_link,
div.dPostsDiv .InsideTextAds .visible_url a  {
  font-size: 100%;
}

div.dPostsDiv div.dPost {
	margin-bottom: 30px;
}

div.dPostsDiv div.dPostBody p {
	font-size: 100%;
}

div.dPostsDiv div.dPostDate,
div.dPostsDiv div.dPostAuthor {
	padding-right: 20px;
	margin-top: 3px;
	text-align: right;
	font-weight: bold;
}

div.RelatedsContainer {
  padding: 0px 10px;
  margin-top: 20px;
}

.imageBlock amp-img {display: inline-block; border: 2px solid #000080;}</style>
    <meta name="description" content="A WAP phone is a mobile phone capable of Internet connectivity. Though a WAP phone can't compare to a computer, it does allow... - mobile wiseGEEK" />

    <meta content="initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" name="viewport" />

    
    <meta http-equiv="imagetoolbar" content="no" />
    <meta http-equiv="content-language" content="en-US">

    <link rel="shortcut icon" href="//www.wisegeek.com/favicon.ico">
            <link rel="canonical" href="//www.wisegeek.com/what-is-a-wap-phone.htm" />
    
    <style>body {opacity: 0}</style><noscript><style>body {opacity: 1}</style></noscript>

  
      </head>

  <body>
  
<!-- Begin comScore Tag -->
<amp-pixel src="//b.scorecardresearch.com/p?c1=2&c2=8556372&cv=2.0&cj=1"></amp-pixel>
<!-- End comScore Tag -->
<div class="HeaderContainer">

  <h1 class="ArticleTitle" property="dc:title">&nbsp;</h1>

</div>

<div class="ContentContainer">

            <div class="InsideTextAds" id="gad-0">
             
        </div>
    
    <!-- google_ad_section_start -->
    <div class="ContentBlock">
                            <p>&nbsp;</p>
    </div>
  <div>
    <div class="dPostsDiv" id="PageArticleCom_discussionPosts-postsContainer_id">
      <div class="dPost" id="post_147564" author_id="39806" stage="confirmed">        </div>

            <span class="gad-cp"></span><span class="gad-cp"></span>

		
	</div>

          </div>
  
  <h2 class="SubSectionTitle">{keyword}</h2>
  <div class="RelatedsContainer">
          <div class="LinkBlock" property="dc:relation">
        
    </div>
      <div class="LinkBlockSeparator"></div>
          <div class="LinkBlock" property="dc:relation">
        <b>{manytext_bing}</b></div>
      <div class="LinkBlockSeparator"></div>
  </div>
</div>

    <amp-lightbox id="imageLigtbox-0" layout="nodisplay">
        <div class="overlay" on="tap:imageLigtbox-0.close"></div>
        <div class="lightbox">
            <div class="allc"><div class="allc">
                <div class="cover">
                    <amp-img
                        src="//images.wisegeek.com/smart-phone.jpg"
                        layout="responsive"
                        width="1000"
                        height="733"
                        alt="A WAP phone is a mobile phone capable of Internet connectivity."
                    ></amp-img>
                    <p>&nbsp; </p></div></div></div>
        </div>
    </amp-lightbox>
    <amp-lightbox id="imageLigtbox-1" layout="nodisplay">
        <div class="overlay" on="tap:imageLigtbox-1.close"></div>
        <div class="lightbox">
            <div class="allc"><div class="allc">
                <div class="cover">
                    <amp-img
                        src="//images.wisegeek.com/woman-at-airport-with-cell-phone.jpg"
                        layout="responsive"
                        width="589"
                        height="800"
                        alt="WAP is an international internet standard specifically developed for mobile devices."
                    ></amp-img>
                    <p>&nbsp; </p>
                </div>
            </div></div>
        </div>
    </amp-lightbox>
    <amp-lightbox id="imageLigtbox-2" layout="nodisplay">
        <div class="overlay" on="tap:imageLigtbox-2.close"></div>
        <div class="lightbox">
            <div class="allc"><div class="allc">
                <div class="cover">
                    <amp-img
                        src="//images.wisegeek.com/person-touching-mobile-phone-near-tablet.jpg"
                        layout="responsive"
                        width="1000"
                        height="704"
                        alt="WAP provides mobile-specific language protocols and scripts."
                    ></amp-img>
                    <p> .</p>
                </div>
            </div></div>
        </div>
    </amp-lightbox>
 
</body>
</html>